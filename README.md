# myFritzingParts

This is a collection of self made fritzing parts which I didn't found anywhere. So DIY was the best solution.


## Parts

### [waveshare 1.3 inch 240x240 TFT](./waveshare_1in3_240x240_tft.fzpz)
A nice little 1.3 inch (240x240pixels) TFT Display from Waveshare with SPI interface with wire connectors. [Specification](https://www.waveshare.com/wiki/1.3inch_LCD_Module)

![waveshare 1.3 inch 240x240 TFT](_assets/waveshare_1in3_240x240_tft.png)
